import 'package:flutter/material.dart';
import 'my_map.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: MyMap(),
    );
  }
}
