import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'dart:async';

class MyMap extends StatefulWidget {
  const MyMap({Key? key}) : super(key: key);

  @override
  State<MyMap> createState() => _MyMapState();
}

class _MyMapState extends State<MyMap> {
  late LatLng userPosition;
  List<Marker> markers = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("My Position")),
      body: FutureBuilder(
        future: checkUserPosition(),
        builder: (BuildContext context, AsyncSnapshot snapshot){
          if(snapshot.hasData){
            return GoogleMap(initialCameraPosition: CameraPosition(
              target: snapshot.data,
              zoom: 15,
            ),
            markers: Set<Marker>.of(markers),);
          }
          else{
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: const <Widget>[
                  Text(
                    'Please wait',
                    style: TextStyle(fontSize: 25, color: Colors.black),
                  ),
                  SizedBox(height: 20),
                  CircularProgressIndicator(),
                ],
              ),
            );
          }
        },
      )
    );
  }

  Marker buildMaker(LatLng pos) {
    MarkerId markerId = const MarkerId('A');
    Marker marker = Marker(
        markerId: markerId,
        position: pos
    );
    return marker;
  }

  Future<LatLng> checkUserPosition() async {
    Location location = Location();
    LocationData userLocation;
    PermissionStatus hasPermission = await location.hasPermission();
    bool active = await location.serviceEnabled();

    if (hasPermission == PermissionStatus.granted && active) {
      userLocation = await location.getLocation();

      userPosition = LatLng(userLocation.latitude as double, userLocation.longitude as double);
    } else {
      //My House
      userPosition = LatLng(11.3351554,106.1098854);
    }

    // Display marker at the user position
    if (markers.isEmpty) {
      markers.add(buildMaker(userPosition));
    } else {
      markers[0] = buildMaker(userPosition);
    }

    setState(() { });

    return userPosition;

  }

}

